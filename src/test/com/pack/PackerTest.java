package com.pack;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.pack.exception.APIException;

public class PackerTest {
	
	@Test
	public void teste() throws APIException {
		 Packer packer = new Packer(); // MyClass is tested
		 List<String> packages = packer.pack("C:\\Users\\cleto\\Desktop\\usecase.txt");
		 
		 assertEquals("4", packages.get(0));
		 assertEquals("-", packages.get(1));
		 assertEquals("2,7", packages.get(2));
		 assertEquals("8,9", packages.get(3));
	}
	
}
