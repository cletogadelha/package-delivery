package com.pack;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.pack.domain.Item;
import com.pack.exception.APIException;

public class Packer {
	
	/**
	 * Method responsible to read the file and for each line create the structure that the engine needs to calculate the best package;
	 * @param absolutePath
	 * @return 
	 * @return
	 * @throws APIException
	 */
	public static List<String> pack(String absolutePath) throws APIException {
		List<String> results = new ArrayList<>();
		try (Stream<String> stream = Files.lines(Paths.get(absolutePath))) {
	        stream.forEach(line -> {
	        	//regex to capture the capacity and items
	        	//the first match will have the capacity and the item, the following matches, just the items
	    		Pattern pattern = Pattern.compile("((\\d+)\\s*:\\s*)*\\((.*?)\\)");
	            Matcher matcher = pattern.matcher(line);
	            
	            List<Item> items = new ArrayList<>();
	            
            	Integer capacity = 0;
	            while(matcher.find()) {
	            	if(matcher.group(2) != null) {
	            		capacity = Integer.parseInt(matcher.group(2));
	            	}
	            	String[] numbers = matcher.group(3).split(",");
	            	
	            	items.add(new Item(Integer.parseInt(numbers[0]), Double.parseDouble(numbers[1]), Double.parseDouble(numbers[2].replaceAll("�", ""))));
	            }
	            
	            //Call the engine to calculate the best package, passing the capacity and items
	            PackageEngine pic = new PackageEngine(capacity, items);
	            try {
	            	results.add(pic.calculatePackage());
	            } catch (APIException e) {
	            	e.printStackTrace();
	            }
	        });
	        results.forEach(System.out::println);
	        return results;
	        //If some problem happens with the file, print the below exception
		} catch (IOException e) {
			throw new APIException("There was a problem when we tried to open you file!");
		}
	}
	
	/*
	 * Receive the absolute path of the file as parameter
	 */
	public static void main(String[] args) throws APIException {
		if(args.length == 0) {
			throw new APIException("No Path Provided");
		}
		
		pack(args[0]);
	}

}
