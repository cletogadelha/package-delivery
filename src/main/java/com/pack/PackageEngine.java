package com.pack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.pack.domain.Item;
import com.pack.exception.APIException;

public class PackageEngine {
	
	private List<Item> items;
	private int capacity;
	private List<List<String>> combinations;
	
	public static final Integer MAX_CAPACITY = 100;

	public PackageEngine(int capacity, List<Item> items) {
		this.capacity = capacity;
		this.items = items;
	}
	
	/**
	 * Method responsible to ask all possible combinations, and from that define what's the best combination for the package
	 * @throws APIException
	 */
	public String calculatePackage() throws APIException {
		
		//if the bag has more capacity than the max permitted one
		if(capacity > MAX_CAPACITY) {
			throw new APIException("Forbidden package weight. Max : 100");
		}
		
		double maxCost = 0, maxWeight = 0;
		List<String> bestCombination = new ArrayList<>();
		
		//filter only the items that fits on the package
		List<Item> filteredItems = items.stream().filter(item -> item.getWeight() <= capacity).collect(Collectors.toList());
		
		//create all posible combinations starting from index 0
		this.combinations = createPossibleCombinations(filteredItems, 0);
		
		for(List<String> combination : this.combinations) {
			double weight = getWeightOfSubset(combination);
			double cost = getCostOfSubset(combination);
			//if the combination fits on the package
			if(weight <= capacity) {
				
				//if the cost of this combination is bigger than the previous one, use it
				if(cost > maxCost) {
					maxWeight = weight;
					maxCost = cost;
					bestCombination = combination;
					//if it's the same cost , test the weight
				} else if ( cost == maxCost ) {
					//if the combination is lighter than the previous one, use it
					if(weight < maxWeight) {
						maxWeight = weight;
						bestCombination = combination;
					}
				}
			}
		};
		
		return printBestCombination(bestCombination);
	}
	
	public String printBestCombination (List<String> combination) {
		return combination.isEmpty() ? "-" : Arrays.stream(combination.get(0).split(",")).collect(Collectors.joining(","));
	}
	
	public double getWeightOfSubset(List<String> subset) {
		return Arrays.stream(subset.get(0).split(",")).map(s -> items.get(Integer.parseInt(s) - 1).getWeight()).reduce(0D, Double::sum);
	}
	
	public double getCostOfSubset(List<String> subset) {
		return Arrays.stream(subset.get(0).split(",")).map(s -> items.get(Integer.parseInt(s) - 1).getCost()).reduce(0D, Double::sum);
	}
	
	//As the maximum amount of items is 15, i followed the approach of create all possible combinations between the items
	//There is a classical approach to solve this kind of problem ( dynamic programming ) but i 
	//believe that for this scenario is more complicated than just create a powerset of possibilities.
	private static List<List<String>> createPossibleCombinations(final List<Item> values, int index) {
		if (index == values.size()) {
			return new ArrayList<>();
		}
		
		Item item = values.get(index);
		
		List<List<String>> subset = createPossibleCombinations(values, index + 1);
		List<List<String>> returnList = new ArrayList<>();
		
		returnList.add(Arrays.asList(item.getIndex().toString()));
		returnList.addAll(subset);
		
		for (List<String> subsetValues : subset) {
			for (String subsetValue : subsetValues) {
				returnList.add(Arrays.asList(item.getIndex() + "," + subsetValue));
			}
		}
		return returnList;
	}

}
