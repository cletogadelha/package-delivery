You can create your jar using ``mvn package``

Run the -jar-with-dependencies variante of the jar created with the following command:

E.G : java -jar package-delivery-0.0.1-SNAPSHOT-jar-with-dependencies.jar PATH_TO_YOUR_FILE


OR

Run directly the jar provided.

E.G : java -jar package.jar PATH_TO_YOUR_FILE